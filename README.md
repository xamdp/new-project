# 2. Docker Basics

## Introduction
Docker is a tool that makes it easier to create, deploy, and run applications using
containers. Containers allows a developer to package up an application with all parts it needs
- such as libraries and other dependencies - ship it all out as one package.

## Importance
- Portability
- Isolation
- Efficiency
- Scalability
